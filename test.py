#encoding: utf-8

import os
import pyinotify
import time

class EventHandler(pyinotify.ProcessEvent):
    """
    Cette classe est utilisée pour prendre en compte les événements déclenchés par inotify.
    Elle va servir à déclencher la copie des fichiers depuis et vers la clef USB.
    """
    def process_IN_CREATE(self, event):
        # Accessing pathname could take some undefined time...
        while True:
            time.sleep(.1)
            if os.path.exists(event.pathname):
                break
        print("Nouveau périphérique USB détecté : %s" % event.pathname)
        if event.pathname.startswith('sd') and event.pathname.endswith('1'):
            print("Tentative de montage du périphérique USB")
            

if __name__=='__main__':
    # Initialisation de la surveillance du répertoire de montage des clefs USB
    watch_manager = pyinotify.WatchManager()
    handler = EventHandler()
    notifier = pyinotify.Notifier(watch_manager, handler)
    # On surveille la création du répertoire de montage
    watch_manager.add_watch('/dev/', pyinotify.IN_CREATE)
    print("En attente...")
    notifier.loop()
    print("Fini !")

