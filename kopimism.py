#encoding: utf-8
"""
Ce module sert à gérer l'autel kopimiste dans son ensemble. Il pourra être découpé en plusieurs
modules si nécessaire.

Il pourra aussi être traduit en d'autres langues ;o)
"""

import os
import pyinotify
import random
import threading
import time

import RPi.GPIO as GPIO

import config

TIME_LAPSE = 3

class Datalove():
    def __init__(self, pathname, led_thread):
        """
        Initializing thread for spreading datalove
        """
        self.pathname = pathname
        self.leds = led_thread

    def run(self):
        """
        Copying files from and to usb drive 
        """
        # Step 1: Set leds to mode 'Starting'
        self.leds.set_mode('STARTING')

        # Step 3: Set leds to mode 'Copying from USB'
        self.leds.set_mode('COPYING_FROM_USB')

        # Step 4: Copy some files from USB drive to disk
        self.get_files()

    def toto(self):
        # Step 5: Set leds to mode 'Copying to USB'
        self.leds.set_mode('COPYING_TO_USB')

        # Step 7: Copy some files from disk to USB drive
        files_to_put = random.randrange(config.MIN_FILES_TO_PUT, config.MAX_FILES_TO_PUT+1)
        print("%s files to put on USB" % files_to_put)
        for number in range(files_to_put):
            #filepath = self.select_file_from(config.DRIVE_FOLDER)
            #self.copy_file(filepath, usb_target)
            pass
        # Step 8: Set leds to mode 'Cleaning'
        self.leds.set_mode('CLEANING')
        # Step 9: Release USB drive and clean files if needed
        #for filepath in self.files_to_delete():
        #    os.remove(filepath)
        time.sleep(TIME_LAPSE)
        # Unmount USB drive
        # Step 10: Set leds to mode 'Waiting'
        self.leds.set_mode('WAITING')

    def get_files(self):
        files_to_get = random.randrange(config.MIN_FILES_TO_GET, config.MAX_FILES_TO_GET+1)
        print("%s files to get from USB" % files_to_get)
        # TODO: Recursive search upon all subdirectories in order to get n filepath
        files = (f for f in os.listdir(self.pathname) if os.path.isfile(f))
        for f in files:
            print(f)


class LedThread(threading.Thread):
    LED_MODES = {
        'NO_LED': 0,
        'WAITING': 10,
        'STARTING': 20,
        'COPYING_FROM_USB': 30,
        'COPYING_TO_USB': 40,
        'CLEANING': 50,
        'TEST': 99,
    }

    def __init__(self):
        """
        Initializing thread for displaying leds
        """
        super(LedThread, self).__init__()
        self.running = True
        self.led_mode = 0
        self.mode_changed = False
        self.timer = 0
        # Initialize modes
        self.leds = config.LEDS
        self.mode_10_value = config.MODE_10_DEFAULT_VALUE
        self.mode_10_speed = config.MODE_10_SPEED
        self.mode_20_led = config.MODE_20_DEFAULT_LED
        self.mode_20_speed = config.MODE_20_SPEED
        # Initialize leds
        GPIO.setmode(GPIO.BOARD)
        for led in self.leds:
            GPIO.setup(led, GPIO.OUT)

    def run(self):
        """
        Displaying leds based on led_mode
        """
        while self.running:
            time.sleep(.1)

            # Shutting down leds is only interesting once
            if self.led_mode==0 and self.mode_changed:
                self.clear_leds()

            # This mode set leds blinking
            if self.led_mode==10 or self.led_mode==30 or self.led_mode==50:
                if self.mode_changed:
                    self.clear_leds()
                    self.mode_10_value = config.MODE_10_DEFAULT_VALUE
                    self.timer = 0
                if time.time() - self.timer > self.mode_10_speed:
                    self.timer = time.time()
                    GPIO.output(11, self.mode_10_value)
                    GPIO.output(13, self.mode_10_value)
                    GPIO.output(15, self.mode_10_value)
                    self.mode_10_value = not self.mode_10_value

            # This mode set leds like a caterpillar
            if self.led_mode==20 or self.led_mode==40:
                if self.mode_changed:
                    self.clear_leds()
                    self.mode_20_led = config.MODE_20_DEFAULT_LED
                    self.timer = 0
                if time.time() - self.timer > self.mode_20_speed:
                    self.timer = time.time()
                    # Shut down old led
                    GPIO.output(self.leds[self.mode_20_led], False)
                    self.mode_20_led += 1
                    if self.mode_20_led>=len(self.leds):
                		self.mode_20_led = 0
                    # Light new led
                    GPIO.output(self.leds[self.mode_20_led], True)

            if self.mode_changed:
                self.mode_changed = False
                print('Led mode : %s' % self.led_mode)

    def clear_leds(self):
        for led in self.leds:
            GPIO.output(led, False)

    def set_mode(self, mode):
        """
        Setting specific mode for leds display
        """
        if mode in self.LED_MODES:
            self.led_mode = self.LED_MODES[mode]
            self.mode_changed = True

    def stop(self):
        """
        Stopping Thread
        """
        # Shutting down leds
        self.running = False
        time.sleep(.1)
        self.clear_leds()


class EventHandler(pyinotify.ProcessEvent):
    """
    Cette classe est utilisée pour prendre en compte les événements déclenchés par inotify.
    Elle va servir à déclencher la copie des fichiers depuis et vers la clef USB.
    """
    def process_IN_CREATE(self, event):
        # Accessing pathname could take some undefined time...
        while True:
            time.sleep(.1)
            if os.path.exists(event.pathname):
                break
        Datalove(event.pathname, led_thread).run()


if __name__=='__main__':
    # Initialisation du thread gérant les leds
    led_thread = LedThread()
    led_thread.start()
    # Affichage d'une animation de base
    led_thread.set_mode('WAITING')

    # On met de côté la gestion du point de montage automatique
    if False:
        # Initialisation de la surveillance du répertoire de montage des clefs USB
        watch_manager = pyinotify.WatchManager()
        handler = EventHandler()
        notifier = pyinotify.Notifier(watch_manager, handler)
        # On surveille la création du répertoire de montage
        watch_manager.add_watch(config.MOUNTING_FOLDER, pyinotify.IN_CREATE)
        notifier.loop()

    # Et on utilise à la place la methode de copie directement
    time.sleep(1)
    Datalove('/home/thomas/projets/hatlab/kopimi/dev/datalove_test', led_thread).run()

    # On arrête proprement le thread gérant les leds
    led_thread.stop()

