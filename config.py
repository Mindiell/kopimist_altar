#dncoding: utf-8

MOUNTING_FOLDER = '/media/thomas'
DRIVE_FOLDER = '/home/thomas/projects/hatlab/kopimi/datalove'

MIN_FILES_TO_GET = 1
MAX_FILES_TO_GET = 3

MIN_FILES_TO_PUT = 1
MAX_FILES_TO_PUT = 3

MAX_FILE_SIZE = 500
AUTHORIZED_EXTENSIONS = (
    'pdf',
)

LEDS = (11,13,15)

MODE_10_DEFAULT_VALUE = True
MODE_10_SPEED = .5

MODE_20_DEFAULT_LED = 2
MODE_20_SPEED = .3

